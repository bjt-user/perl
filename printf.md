The `printf` syntax is different from `bash`.

```
#!/bin/env perl

my $myvar = "works";

printf("printf %s\n", "$myvar");
```

from the docs https://perldoc.perl.org/functions/printf \
"Don't fall into the trap of using a printf when a simple print would do. The print is more efficient and less error prone."
