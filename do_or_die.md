In some perl scripts you will find the key words `do ... or die "...";`.

```
do $myperlscript or die "error executing script";
```

https://perldoc.perl.org/functions/do

## major pitfall

If do can read the file but cannot compile it, it returns undef and sets an error message in $@.\
If do cannot read the file, it returns undef and sets $! to the error.\
Always check $@ first, as compilation could fail in a way that also sets $!.\
**If the file is successfully compiled, do returns the value of the last expression evaluated.**

This is a **pitfall**.\
So it seems to be common to write `$dummy=1;` or just `1;` at the end of a script file or module that is loaded.

Putting something like `$myvar=0;` at the end of the file will raise an exception.
