#### check for empty strings

```
#!/bin/env perl

my $mystring;

if ($mystring) {
  printf "this string has a value.\n";
} else {
  printf "this string is empty.\n";
}
```
It will output `this string is empty`.\
It will also output `this string is empty` when you do this assignment: `my $mystring = "";`.

It will say `this string has a value` when you do this assignment: `my $mystring = "b";`.
