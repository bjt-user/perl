Since `print` is more efficient and error prone that `printf` due to docs you should usually use `print`.

https://perldoc.perl.org/functions/printf \
*Don't fall into the trap of using a printf when a simple print would do. The print is more efficient and less error prone.*

#### example

```
#!/bin/env perl

my $mystring = "something";

print "the string \$mystring is " . $mystring . "\n";

print $mystring;
```

`print` doesn't do a line break at the end.\
You concatenate strings with the `.` operator.\
If you want to print the name of a variable you have to escape the `$` with a `\` otherwise the value of the variable will be printed.
