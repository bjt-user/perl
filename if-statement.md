#### comparing strings

Comparing strings with `==`, `!=`, and so on does **NOT** work.

*Use `lt`, `gt`, `eq`, `ne`, and `cmp` as appropriate for string comparisons:\
Binary eq returns true if the left argument is stringwise equal to the right argument.\
Binary ne returns true if the left argument is stringwise not equal to the right argument.\
Binary cmp returns -1, 0, or 1 depending on whether the left argument is stringwise less than, equal to, or greater than the right argument.\
Binary ~~ does a smartmatch between its arguments.*


Example:
```
#!/bin/env perl

my $randomflag = "hello";

if ($randomflag eq "hello") {
  printf "Bingo.\n";
}

if ("$randomflag" eq "nothello") {
  printf "Variable has value \"nothello\"";
}
```

#### multiple conditions

The syntax for more than one condition in an if-statement:
```
#!/bin/env perl

my $firststring = "hello";
my $secondstring = "hello";

if ($firststring eq "hello" && $secondstring eq "hello") {
  printf "Both strings say hello.\n";
}

if ($firststring eq "hello" || $secondstring eq "hello") {
  printf "At least one of the variables contains the string hello.\n"
}
```
