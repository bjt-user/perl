Functions in perl are called `sub`s. ("subroutines")

This will print "hello" twice:
```
#!/bin/env perl

sub print_hello {
  print "hello\n";
}

print_hello();
print_hello();
```

You can also call the `sub` without the brackets like this:
```
print_hello;
```

How to pass arguments to a `sub`:
```
#!/bin/env perl

sub print_twice {
  my $number_of_args=@_;
  my $first_argument=@_[0];
  printf ("%s\n", $first_argument);
  printf ("%s\n", $first_argument);
  printf ("%i\n", $number_of_args);
}

print_twice("sausage", "bread", "ok");
```

#### `@_`

Within a subroutine the array `@_` contains the number of parameters passed to that subroutine.


#### ampersand before sub call

https://alvinalexander.com/perl/call-perl-subroutines-sub-function-ampersand/

Usually you should put an `&` infront of the function when calling it before it is declared.\
But a lot of the time it will work even without a `&`, seems to be a sketchy thing.

So it is useful to know what the `&` in front of functions does if you see a perl script from someone else,\
but I would just declare subs before calling them.
