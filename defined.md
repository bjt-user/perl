#### my findings

My findings after some experimentation with `use strict` and the `defined` keyword:\
You **can't** use those two together in a script.\
When you don't define a variable but use it in a `define` function `use strict` will make the script not compile at all.

An empty string (like `my example = ""`) counts also as `defined`.

Experiment: Will a failed file opening return an `undef` variable?\
Yes, but if the file exists the return value also counts as `undef`.

```
#!/bin/env perl

use strict;

my $openresult;

$openresult = open("nonexistingfile.txt");

if (defined $openresult) {
  print "variable \$openresult is defined.\n";
}
```
=> this perl script will not print anything to the screen because `$openresult` is undefined.

***
#### official documentation
from the official docs:\
https://perldoc.perl.org/functions/defined

Returns a Boolean value telling whether EXPR has a value other than the undefined value `undef`.\
Many operations return undef to indicate failure, end of file, system error, uninitialized variable, and other exceptional conditions.\
This function allows you to distinguish undef from other values. (A simple Boolean test will not distinguish among undef, zero, the empty string, and "0", which are all equally false.)\
Note that since undef is a valid scalar, its presence doesn't necessarily indicate an exceptional condition: pop returns undef when its argument is an empty array, or when the element to return happens to be undef.

Use of defined on aggregates (hashes and arrays) is no longer supported.

Note: Many folks tend to overuse defined and are then surprised to discover that the number 0 and "" (the zero-length string) are, in fact, defined values.
