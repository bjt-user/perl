general definition of "locale": https://en.wikipedia.org/wiki/Locale_(computer_software)

`locale` - Perl pragma to use or avoid POSIX locales for built-in operations\
https://perldoc.perl.org/locale

Looks like you can do `use locale;` to enable POSIX locales.\
And `no locale;` to avoid them.
