A `scalar` seems to be a dynamic datatype like `var` or `auto` in other languages.

To declare a `scalar` use the `$` symbol.

```
$my_scalar="ok";
```
```
$my_scalar=1;
```

https://perldoc.perl.org/perlintro#Perl-variable-types

"There is no need to pre-declare your variable types, but you have to declare them using the my keyword the first time you use them.\
(This is one of the requirements of `use strict;`.)"
