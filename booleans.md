https://www.geeksforgeeks.org/perl-boolean-values/

*In most of the programming language True and False are considered as the boolean values.\
But Perl does not provide the type boolean for True and False.*

So you should probably just use `0` for false and `1` for true.

https://perldoc.perl.org/perlintro#Perl-variable-types

*Perl has three main variable types: scalars, arrays, and hashes.*
