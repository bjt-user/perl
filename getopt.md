#### Getopt::Long
https://perldoc.perl.org/Getopt::Long

`Getopt::Long` is a module.

You have to load the module at the beginning of the script like this:
```
use Getopt::Long;
```

The `Getopt::Long` module implements an extended `getopt` function called `GetOptions()`.

```
#!/bin/env perl

use Getopt::Long;

GetOptions ("length=i" => \$length,    # numeric
            "file=s"   => \$data,      # string
            "verbose"  => \$verbose);  # flag

printf("Variable length is %i\n", $length);
printf("variable verbose is %s\n", $verbose);
printf("variable data is %s\n", $data);
```

Output:
```
~/perl $ ./getoptions.pl --length 12
Variable length is 12
~/perl $ ./getoptions.pl -l 12
Variable length is 12
~/perl $ ./getoptions.pl -length 12
Variable length is 12
~/perl $ ./getoptions.pl --l 12
Variable length is 12
~/perl $ ./getoptions.pl --verbose -l 10 --file test.txt
Variable length is 10
variable verbose is 1
variable data is test.txt
~/perl $ ./getoptions.pl -l 10 --file test.txt
Variable length is 10
variable verbose is
variable data is test.txt
```
