`say` is like `print` but it adds a newline at the end.

To use `say` you need type this at the beginning of your script:
```
use feature "say";
```

Then you can do
```
say("something");
```
or
```
say "something";
```
