Declare a string array and print all elements with a foreach loop:
```
#!/bin/env perl

@myarray;

@myarray=("apple","banana","orange");

foreach $element (@myarray) {
  printf("%s\n", $element);
}
```
Instead of a `foreach` loop you can also use a `for` loop, but I think `foreach` is more clear.

#### get last array element
```
print $myarray[-1];
```
or (by using the length of the array with the `scalar` function)
```
my @myarray = ("one", "two", "three");

my $lastindex = scalar @myarray - 1;

print $myarray[$lastindex];
```

#### how to get the length of an array
use the `scalar` function\
https://perldoc.perl.org/functions/scalar
```
#!/bin/env perl

my @myarray = ("one", "two", "three");

print scalar @myarray;

```
or (with assigning the length to variable)
```
#!/bin/env perl

my @myarray = ("one", "two", "three");

my $length = scalar @myarray;

print $length
```

#### What is the difference between `$array[1]` and `@array[1]`?
https://perldoc.perl.org/perlfaq4#What-is-the-difference-between-%24array%5B1%5D-and-%40array%5B1%5D%3F

The difference is the sigil, that special character in front of the array name.\
The $ sigil means "exactly one item", while the @ sigil means "zero or more items".\
The $ gets you a single scalar, while the @ gets you a list.

The confusion arises because people incorrectly assume that the sigil denotes the variable type.

The `$array[1]` is a single-element access to the array. It's going to return the item in index 1 (or undef if there is no item there). If you intend to get exactly one element from the array, this is the form you should use.

The `@array[1]` is an array slice, although it has only one index. You can pull out multiple elements simultaneously by specifying additional indices as a list, like `@array[1,4,3,0]`.
