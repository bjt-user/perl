Syntax check:
perl -c script.pl

To get warnings do this
```
use warnings;
```
at the beginning of the script.

If you get warnings that are unjustified put this infront of the problematic command:
```
no warnings 'once';
```

`use strict;` forces each variable to be declared before use by pulling in the strict module.\
You implement this typically by putting `use strict;` at the start of the script.\
`use strict;` does not produce warnings, it does not compile at all if you did not declare variables before usage.

# DEBUGGING
https://perldoc.perl.org/perldebug
```
perl -d script.pl
```
`q` to quit the debugger\
`h` for help\
`n` next, steps over subs\
`s` step into subs\
`r` Continue until the return from the current subroutine. Dump the return value if the PrintRet option is set (default).\
`v` show some code and at which line you are\
`p $myvar` to print the content of a variable.\
`l 10-30` will show the code from line 10 to 30 and an arrow that points to the current line\
`<enter>` repeat the last `n` or `s` command\
`.` prints the line that is going to run next\
`L b` list breakpoints

It's stopped at the first line of executable code and is waiting for input.\
It shows the next line of code, that is not executed yet.

After you entered a `n` or `s` command you can just hit `<enter>` to execute the next line.

#### setting breakpoints

You can do `b [linenumber]` and then `c` for "continue" and it will take you in front of that line.\
You can do `b [nameofasub]` and then `c` for "continue" and it will take you to the first line of that sub.

#### show contents of an array

If you type `p $myarray` you will only get a hex code. (probably the address where that array is stored in RAM)\
But if you type `p @{$myarray}` you see the contents of that array (without separator)
